Scenario = "MATS/DCS_gains_SNR_%d_M_%d_%s.mat";

set(groot,'defaultAxesFontSize',14);
set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextFontSize',14);
set(groot,'defaulttextinterpreter','latex');
%set(groot,'defaultLegendFontSize',14);
%set(groot,'defaultLegendInterpreter','latex');

What = "CalibE";

SNRs = 0;
Meas = [6:100];


win = [];
means = [];
vars = [];

for i=Meas
  try
    load(sprintf(Scenario, SNRs, i, What));
    win = [win, 1];
    means = [means, mean(DCSgain)];
    vars = [vars, var(DCSgain)]; 
  catch
    disp("Strange things");
    win = [win, 0];
  end
end

%prepare interpolation space
%PDFi = linspace(min(Sx(:)), max(Sx(:)));
%CDFi = linspace(min(Sy(:)), max(Sy(:)));



%figure(1);
%hold on;
%grid on;
%box on;
%xlabel("Channel measurements");
%ylabel("Beamforming Gain [dBi]");
%title("Achievable rate histogram");
%xlim([min(Meas), max(Meas)]);
%for i=[1:length(Meas)]
%  plot3(Meas(i)*ones(size(Px)), Sx(i,:), Sy(i,:), 'linewidth', 1.5);
%end
%view(45,35);


figure(3);
hold on;
plot(Meas(win==1), means, 'linewidth', 1.5);
box on;
grid on;
xlabel("Number of measurements",'Interpreter','Latex');
ylabel("Mean beamforming gain [dBi]",'Interpreter','Latex');

figure(4);
hold on;
plot(Meas(win==1), sqrt(vars), 'linewidth', 1.5);
box on;
grid on;
xlabel("Number of measurements",'Interpreter','Latex');
ylabel("Beamforming gain STD [dBi]",'Interpreter','Latex');

%figure(3);
%hold on;
%grid on;
%box on;
%xlabel("Channel measurements");
%ylabel("Beamforming Gain [dBi]");
%title("Achievable rate eCDF");
%xlim([min(Meas), max(Meas)]);
%for i=[1:length(Meas)]
%  plot3(Meas(i)*ones(size(Px)), Sx(i,:), cumsum(Sy(i,:)), 'linewidth', 1.5);
%end
%view(45,35);