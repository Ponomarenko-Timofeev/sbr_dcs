function [means, vars, win, DCSG] = loadem(Scenario, What, SNR, Meas)

  win = [];
  means = [];
  vars = [];
  DCSG = cell([length(Meas), 1]);
  k = 1;

  for i=Meas 
    try
      load(sprintf(Scenario, SNR, i, What));
      means = [means, mean(DCSgain)];
      vars = [vars, var(DCSgain)]; 
      DCSG{k} = DCSgain;
      win = [win, 1];
    catch
      disp(strcat("Missing data:", What,"@",num2str(i)," meas, ", num2str(SNR)," dB"));
      win = [win, 0];
    end
    k=k+1;
  end
  
end
