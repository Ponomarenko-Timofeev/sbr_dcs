close all;
clear all;

# Install and load package for nice plots
pkg install https://gitty.jumpingcrab.com/Aleksei/nice/archive/unstable.tar.gz;
pkg load nice;

graphics_toolkit qt;

ofmt = ".svg";

%set(groot,'defaultAxesFontSize',14);
%set(groot,'defaultAxesTickLabelInterpreter','latex');
%set(groot,'defaulttextFontSize',14);
%set(groot,'defaulttextinterpreter','latex');
% UNCOMMENT BELOW IF USING MATLAB
%set(groot,'defaultLegendFontSize',14);
%set(groot,'defaultLegendInterpreter','latex');

colorm = viridis;
%colorm = colorm(end:-1:1,:);

DEF_M = 28;

cmm = lines();

%cmm = [cmm(1,:); cmm(2,:); cmm(3,:); cmm(4,:); cmm(5,:); cmm(6,:); cmm(7,:); cmm(8,:)];

lsarr = {"-"; "--"; ":"; "-."};
lcarr = {" "; " "; " "; " "; " "; " "; " "};
SNRs = {-10, 0, 10};
snrs = [-10:30];
Ms = [6:100];
MS = 28;

%synth - Pure geometry-based model initially used by Nitin
%CSCalibr - WI based, empty room with walls made of PEC
%CalibE - WI based, empty room with walls made of brick
%Human_in_a_room - WI based, brick room with an undressed human
%FleeceNew - WI based, brick room with fleece-clothed human
%Naked_subway - Naked human, surrounded by blockers in a train coach
%Fleece_subway - Dressed human in fleece, surrounded by blockers in a coach

fontss = 18;

SCEs = {"synth", "CSCalibr", "CalibE", "Human_in_a_room", "FleeceNew", "Naked_subway", "Fleece_subway"};
Legss = {"Geometry", "Room [PEC]", "Room [Brick]", "Human [No apparel]", "Human [Fleece]", "Subway [No apparel]", "Subway [Fleece]"};

scl = length(SCEs);
snrl = length(SNRs);

Pref = 'MATS/';

legs = [];

M = cell([length(SCEs), length(SNRs)]);
V = cell([length(SCEs), length(SNRs)]);
W = cell([length(SCEs), length(SNRs)]);
DGSs = cell([length(SCEs), length(SNRs)]);

nf1 = nicefig("fontsize",fontss, 'cmap', cmm);
nf2 = nicefig("fontsize",fontss, 'cmap', cmm);

for i=1:scl
  for j=1:snrl
    [m, v, w, DGS] = loadem(strcat(Pref,"DCS_gains_SNR_%d_M_%d_%s.mat"),...
                              SCEs{i},SNRs{j},Ms);

    M{i,j} = m;
    V{i,j} = v;
    W{i,j} = w;
    DGSs{i,j} = DGS;
    
    hold on;
    grid on;
    box on;
    nf1 = niceplot(nf1, Ms(w==1), m, strcat(lcarr{i},lsarr{j}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(Ms), max(Ms)]);
    
    hold on;
    grid on;
    box on;
    nf2 = niceplot(nf2, Ms(w==1), sqrt(v), strcat(lcarr{i},lsarr{j}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(Ms), max(Ms)]);
    
    if sum(w) > 0
      legs = [legs; strcat(Legss{i},"@",num2str(SNRs{j})," dB")];
    end
    
  end
end

nf1 = niceplot(nf1, Ms, 22.0530*ones(size(Ms)), 'linewidth', 2, 'm-.');
legs = [legs; "Room [PEC]@Perfect CSI"];
nf1 = niceplot(nf1, Ms, 20.1418*ones(size(Ms)), 'linewidth', 2, 'b-.');
legs = [legs; "Subway [Fleece]@Perfect CSI"];

niceleg(nf1, 'location', 'eastoutside', 'orientation', 'vertical', 'numcolumns', length(SNRs), legs);
xlabel("Number of measurements");
ylabel("Beam-forming gain mean [dBi]");
niceprint(nf1, strcat("Gain mean over M", ofmt));

niceleg(nf2, 'location', 'eastoutside', 'orientation', 'vertical', 'numcolumns', length(SNRs), legs);
xlabel("Number of measurements");
ylabel("Beam-forming gain STD [dBi]");
niceprint(nf2, strcat("Gain variance over M", ofmt));

%% Achievable spectral efficiency plots
nf1 = nicefig("fontsize",fontss);
nf2 = nicefig("fontsize",fontss);

for i=1:scl
  for j=1:snrl
    [m, v, w, DGS] = loadem(strcat(Pref,"DCS_gains_SNR_%d_M_%d_%s.mat"),...
                              SCEs{i},SNRs{j},Ms);

    m = 10.^(m ./ 10);
    
    SNRl = 10.^(j / 10);
    
    m = log2(1+m*SNRl);
    
    M{i,j} = m;
    V{i,j} = v;
    W{i,j} = w;
    DGSs{i,j} = DGS;
    
    hold on;
    grid on;
    box on;
    nf1 = niceplot(nf1, Ms(w==1), m, strcat(lcarr{i},lsarr{j}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(Ms), max(Ms)]);
    
    hold on;
    grid on;
    box on;
    nf2 = niceplot(nf2, Ms(w==1), sqrt(v), strcat(lcarr{i},lsarr{j}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(Ms), max(Ms)]);
    
    if sum(w) > 0
      legs = [legs; strcat(Legss{i},"@",num2str(SNRs{j})," dB")];
    end
    
  end
end

niceleg(nf1, 'location', 'eastoutside', 'orientation', 'vertical', 'numcolumns', length(SNRs), legs);
xlabel("Number of measurements");
ylabel("Achievable rate mean [bps/Hz]");
niceprint(nf1, strcat("Rate mean over M", ofmt));

niceleg(nf2, 'location', 'eastoutside', 'orientation', 'vertical', 'numcolumns', length(SNRs), legs);
xlabel("Number of measurements");
ylabel("Achievable rate STD [bps/Hz]");
niceprint(nf2, strcat("Rate variance over M", ofmt));


legs = []

M2 = cell([scl,1]);
V2 = cell([scl,1]);
W2 = cell([scl,1]);
DGSs2 = cell([scl,1]);

nf3 = nicefig("fontsize",fontss);
nf4 = nicefig("fontsize",fontss);

for i=1:scl
    [m, v, w, DGS] = loadesnr(strcat(Pref,"DCS_gains_SNR_%d_M_%d_%s.mat"),...
                              SCEs{i},snrs,MS);

    
    M2{i} = m;
    V2{i} = v;
    W2{i} = w;
    DGSs2{i} = DGS;

    figure(3);
    hold on;
    grid on;
    box on;
    nf3 = niceplot(nf3, snrs(w==1), m, strcat(lcarr{i}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(snrs), max(snrs)]);
    
    figure(4);
    hold on;
    grid on;
    box on;
    nf4 = niceplot(nf4, snrs(w==1), sqrt(v), strcat(lcarr{i}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(snrs), max(snrs)]);
    
    if sum(w) > 0
      legs = [legs; strcat(Legss{i})];
    end
end

nf3 = niceplot(nf3, snrs, 22.0530*ones(size(snrs)), 'linewidth', 2, 'm-.');
legs = [legs; "Room [PEC]@Perfect CSI"];
nf3 = niceplot(nf3, snrs, 20.1418*ones(size(snrs)), 'linewidth', 2, 'b-.');
legs = [legs; "Subway [Fleece]@Perfect CSI"];




niceleg(nf3, 'location', 'southeast', legs);
xlabel("Channel matrix SNR [dB]");
ylabel("Beam-forming gain mean [dBi]");
niceprint(nf3, strcat("Gain mean over SNR", ofmt));

niceleg(nf4, 'location', 'northeast', legs);
xlabel("Channel matrix SNR [dB]");
ylabel("Beam-forming gain STD [dBi]");
niceprint(nf4, strcat("Gain variance over SNR", ofmt));

%% Achievable rate over SNR
nf3 = nicefig("fontsize",fontss);
nf4 = nicefig("fontsize",fontss);

for i=1:scl
    [m, v, w, DGS] = loadesnr(strcat(Pref,"DCS_gains_SNR_%d_M_%d_%s.mat"),...
                              SCEs{i},snrs,MS);

                              
    m = 10.^(m ./ 10);
    
    snrsl = 10.^(snrs / 10);
    
    m = log2(1+m.*snrsl);
                              
    M2{i} = m;
    V2{i} = v;
    W2{i} = w;
    DGSs2{i} = DGS;

    figure(3);
    hold on;
    grid on;
    box on;
    nf3 = niceplot(nf3, snrs(w==1), m, strcat(lcarr{i}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(snrs), max(snrs)]);
    
    figure(4);
    hold on;
    grid on;
    box on;
    nf4 = niceplot(nf4, snrs(w==1), sqrt(v), strcat(lcarr{i}), 'linewidth', 2, 'Color', cmm(i,:));
    xlim([min(snrs), max(snrs)]);
    if sum(w) > 0
      legs = [legs; strcat(Legss{i})];
    end
end

niceleg(nf3, 'location', 'southeast', legs);
xlabel("Channel matrix SNR [dB]");
ylabel("Achievable rate mean [dBi]");
niceprint(nf3, strcat("Rate mean over SNR", ofmt));

niceleg(nf4, 'location', 'northeast', legs);
xlabel("Channel matrix SNR [dB]");
ylabel("Achievable rate STD [dBi]");
niceprint(nf4, strcat("Rate variance over SNR", ofmt));

scesels = [1:7];

nf5 = nicefig("fontsize",fontss);
hold on;
for scesel=scesels
  for i=1:snrl
    m = M{scesel, i};
    v = V{scesel, i};
    w = W{scesel, i};
    nf5 = niceplot3(nf5, ones([1,length(m)])*SNRs{i}, Ms(w==1), m, lcarr{scesel}, 'linewidth', 2);
  end

  m = M2{scesel};
  v = V2{scesel};
  w = W2{scesel};
  nf5 = niceplot3(nf5, snrs(w==1), ones([1, length(m)])*28, m, lcarr{scesel}, 'linewidth', 2);

  grid on;
  box on;

  %% Prepare for fitting
  Mt = M(scesel, :);
  Wt = W(scesel, :);

  Mall = [];
  Mv = [];
  SNRve = [];

  for i=2:length(Mt)
    Mall = [Mall, Mt{1,i}];
    Mv = [Mv, Ms(Wt{1,i} == 1)];
    SNRve = [SNRve, SNRs{i} * ones(size(Mt{1,i}))];
  end
end

for i=1:snrl
  for j=1:scl
    temp = DGSs(j,i);
    temp = cell2mat(temp{:});
    times = [];

    for k=Ms
      times = [times, ones([1, 3600])*k];
    end

    nf = nicefig("fontname", "Latin Modern Roman", "fontsize",fontss, "cmap", colorm);
    nicewfall(nf, temp, times', 80, 100);
    box on;
    grid on;
    xlim([min(temp),max(temp)]);
    ylim([min(times),max(times)]);
    xlabel("Beamforming gain [dBi]");
    ylabel("Number of neasurements");
    nicebar(nf);
    title(strcat(Legss{j},"@",num2str(SNRs{i})," dB"));
    niceprint(nf, strcat(Legss{j},"@",num2str(SNRs{i})," dB", ofmt));
  end
end


for j=1:scl
  temp = DGSs2(j);
  temp = cell2mat(temp{:});
  times = [];

  for k=snrs
    times = [times, ones([1, 3600])*k];
  end

  nf = nicefig("fontname", "Latin Modern Roman", "fontsize",fontss, "cmap", colorm);
  nicewfall(nf, temp, times', 80, 40);
  box on;
  grid on;
  xlim([min(temp),max(temp)]);
  ylim([min(times),max(times)]);
  xlabel("Beamforming gain [dBi]");
  ylabel("H matrix SNR [dB]");
  nicebar(nf);
  title(strcat(Legss{j},"@",num2str(MS)," measurements"));
  niceprint(nf, strcat(Legss{j},"@",num2str(MS)," measurements", ofmt));
end

