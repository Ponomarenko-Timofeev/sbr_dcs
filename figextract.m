function [Px,Py] = figextract(whaf)
openfig(whaf, 'invisible');

fig = gcf;

ax = fig.Children;
[~, lines] = ax.Children;

lines;

x = lines(2).XData;
y = lines(2).YData;

X = linspace(min(x), max(x), 500);

[Xu, ic] = unique(x);
y = y(ic);

Yi = interp1(Xu, y, X);

Py = diff(Yi);
Px = X(1:end-1);
close(gcf);
end