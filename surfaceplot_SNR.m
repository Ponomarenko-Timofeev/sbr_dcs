Scenario = "MATS/DCS_gains_SNR_%d_M_%d_%s.mat";

set(groot,'defaultAxesFontSize',14);
set(groot,'defaultAxesTickLabelInterpreter','latex');
set(groot,'defaulttextFontSize',14);
set(groot,'defaulttextinterpreter','latex');
set(groot,'defaultLegendFontSize',14);
set(groot,'defaultLegendInterpreter','latex');

Sx = [];

What = "CalibE";

SNRs = [-10:40];
Meas = 28;

win = [];
means = [];
vars = [];

for i=SNRs
  try
    load(sprintf(Scenario, i, Meas, What));
    win = [win, 1];
    means = [means, mean(DCSgain)];
    vars = [vars, var(DCSgain)]; 
  catch
    disp("Strange things");
    win = [win, 0];
  end
end

% Make data
%msy = min(Sy(:));
%mul = 1 / msy;




%prepare interpolation space
%PDFi = linspace(min(Sx(:)), max(Sx(:)));
%CDFi = linspace(min(Sy(:)), max(Sy(:)));



%figure(1);
%hold on;
%grid on;
%box on;
%xlabel("Measurement SNR [dB]",'Interpreter','Latex');
%ylabel("Beamforming Gain [dBi]",'Interpreter','Latex');
%title("Achievable rate histogram");
%xlim([min(SNRs), max(SNRs)]);
%for i=[1:length(SNRs)]
%  plot3(SNRs(i)*ones(size(Px)), Sx(i,:), Sy(i,:), 'linewidth', 1.5);
%end
%view(27,18);

%means = [];
%vars = [];

%for i=[1:length(SNRs)]
%  means = [means, mean(Sx)];
%  vars = [vars, var(Sx)]; 
%end

figure(1);
hold on;
plot(SNRs(win == 1), means, 'linewidth', 1.5);
box on;
grid on;
xlabel("Measurement SNR [dB]",'Interpreter','Latex');
ylabel("Mean beamforming gain [dBi]",'Interpreter','Latex');

figure(2);
hold on;
plot(SNRs(win == 1), sqrt(vars), 'linewidth', 1.5);
box on;
grid on;
xlabel("Measurement SNR [dB]",'Interpreter','Latex');
ylabel("Beamforming gain STD [dBi]",'Interpreter','Latex');

%figure(3);
%hold on;
%grid on;
%box on;
%xlabel("Measurement SNR [dB]",'Interpreter','Latex');
%ylabel("Beamforming Gain [dBi]",'Interpreter','Latex');
%title("Achievable rate eCDF");
%xlim([min(SNRs), max(SNRs)]);
%for i=[1:length(SNRs)]
%  plot3(SNRs(i)*ones(size(Px)), Sx(i,:), cumsum(Sy(i,:)), 'linewidth', 1.5);
%end
%view(27,18);