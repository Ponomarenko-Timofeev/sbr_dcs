function [means, vars, win, DCSG] = loadesnr(Scenario, What, SNRs, Meas)

  win = [];
  means = [];
  vars = [];
  DCSG = cell([length(SNRs), 1]);
  k = 1;
  for i=SNRs
    try
      load(sprintf(Scenario, i, Meas, What));
      means = [means, mean(DCSgain)];
      vars = [vars, var(DCSgain)]; 
      DCSG{k} = DCSgain;
      win = [win, 1];
    catch
      disp(strcat("Missing data:", What,"@",num2str(i)," dB, ", num2str(Meas)," meas"));
      win = [win, 0];
    end
    k=k+1;
  end
end
